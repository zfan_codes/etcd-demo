package main

import (
	"context"
	"fmt"
	"go.etcd.io/etcd/clientv3"
	"time"
)

func main() {
	var (
		config clientv3.Config
	)

	config = clientv3.Config{
		Endpoints:   []string{"127.0.0.1:2379"},
		DialTimeout: 5 * time.Second, // 超时时间
	}

	// 建立连接
	client, err := clientv3.New(config)
	if err != nil {
		fmt.Println(err)
		return
	}

	// 用于读取etcd键值对
	kv := clientv3.NewKV(client)

	// 删除job3
	kv.Put(context.TODO(), "/cron/jobs/job5", "bye-bye")
	resp, err := kv.Delete(context.TODO(), "/cron/jobs/job5", clientv3.WithPrevKV())
	if err != nil {
		fmt.Println(err)
		return
	}

	if len(resp.PrevKvs) != 0 {
		for _, ev := range resp.PrevKvs {
			fmt.Println("被删除之前:", string(ev.Key), string(ev.Value))
		}
	}

	fmt.Println("被删除的数量", resp.Deleted)
}
