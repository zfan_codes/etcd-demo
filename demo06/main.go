package main

import (
	"context"
	"fmt"
	"go.etcd.io/etcd/clientv3"
	"time"
)

func main() {
	var (
		config clientv3.Config
	)

	config = clientv3.Config{
		Endpoints:   []string{"127.0.0.1:2379"},
		DialTimeout: 5 * time.Second, // 超时时间
	}

	// 建立连接
	client, err := clientv3.New(config)
	if err != nil {
		fmt.Println(err)
		return
	}

	// 申请一个租约
	lease := clientv3.NewLease(client)

	// 申请一个10秒的租期
	leaseResp, err := lease.Grant(context.TODO(), 10)
	if err != nil {
		fmt.Println(err)
		return
	}

	// 取到租约ID
	leaseId := leaseResp.ID

	// 自动续租（不停地往管道中扔租约信息）
	keepRespChan, err := lease.KeepAlive(context.TODO(), leaseId)
	if err != nil {
		fmt.Println(err)
	}

	// 处理续租应答协程
	go func() {
		for {
			select {
			case keepResp := <-keepRespChan:
				if keepResp == nil {
					fmt.Println("租约已经失效了")
					goto END
				} else {
					fmt.Println("收到自动续租应答：", keepResp.ID)
				}

			}
		}
	END:
	}()

	// 终止租约(定时器)
	time.AfterFunc(10*time.Second, func() {
		fmt.Println("删除租期")
		lease.Revoke(context.TODO(), leaseId)
	})

	kv := clientv3.NewKV(client)

	// 新建一个kv，让他和租约关联起来，10秒自动到期
	resp, err := kv.Put(context.TODO(), "/cron/lock/job1", "--", clientv3.WithLease(leaseId))
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println("写入成功", resp.Header.Revision)

	// 定时查看key是否过期
	for {
		getResp, err := kv.Get(context.TODO(), "/cron/lock/job1")
		if err != nil {
			fmt.Println(err)
			continue
		}

		if getResp.Count == 0 {
			fmt.Println("kv过期了")
			break
		} else {
			fmt.Println("还没有过期")
		}
		time.Sleep(time.Second * 2)
	}

}
