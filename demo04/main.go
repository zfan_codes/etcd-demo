package main

import (
	"context"
	"fmt"
	"go.etcd.io/etcd/clientv3"
	"time"
)

func main() {
	var (
		config clientv3.Config
	)

	config = clientv3.Config{
		Endpoints:   []string{"127.0.0.1:2379"},
		DialTimeout: 5 * time.Second, // 超时时间
	}

	// 建立连接
	client, err := clientv3.New(config)
	if err != nil {
		fmt.Println(err)
		return
	}

	// 用于读取etcd键值对
	kv := clientv3.NewKV(client)

	// 读取/cron/jobs/为前缀的所有key
	getResp, err := kv.Get(context.TODO(), "/cron/jobs/", clientv3.WithPrefix())
	if err != nil {
		fmt.Println(err)
		return
	}

	// 获取成功，遍历所有的KV值
	maps := make(map[string]string)
	for _, ev := range getResp.Kvs {
		maps[string(ev.Key)] = string(ev.Value)
	}

	fmt.Println("结果", maps)
}
