package main

import (
	"context"
	"fmt"
	"go.etcd.io/etcd/clientv3"
	"time"
)

func main() {
	var (
		config clientv3.Config
	)

	config = clientv3.Config{
		Endpoints:   []string{"127.0.0.1:2379"},
		DialTimeout: 5 * time.Second, // 超时时间
	}

	// 建立连接
	client, err := clientv3.New(config)
	if err != nil {
		fmt.Println(err)
		return
	}

	// 用于读取etcd键值对
	kv := clientv3.NewKV(client)

	// 读取值
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second) // 设置超时时间防止超时
	resp, err := kv.Get(ctx, "/cron/jobs/job3")
	cancel()

	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println("打印kv数组", resp.Kvs)
	fmt.Println("获取kv数组数量", resp.Count)
	// [key:"/cron/jobs/job3" create_revision:8 mod_revision:13 version:6 value:"bye" ]
	// create_revision  创建时的版本号
	// mod_revision		最后修改的版本号
	// version			修改的次数

	// GET 可用参数
	// clientv3.WithCountOnly()		只返回kv数组的个数
	// clientv3.WithPrefix()		返回该前缀的所有的key
	// clientv3.WithFromKey()		从key开始前后扫描
	// clientv3.WithLimit(5)		限制数量
}
